import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueCookies from "vue3-cookies";
import "./auth";
import "./tailwind.css";

const app = createApp(App);

app.config.warnHandler = function (msg, vm, trace) {
  return null;
};

app.use(VueCookies);
app.use(store);
app.use(router);
app.mount("#app");
