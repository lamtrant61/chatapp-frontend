import axios from "axios";
import store from "./store/index.js";
import Cookies from "js-cookie";

axios.defaults.headers.common = {
  Authorization: `Bearer ${store.getters.accessToken}`,
};
axios.interceptors.response.use(
  async (response) => {
    const userData = Cookies.get("userData");
    if (userData) {
      const userJson = JSON.parse(userData);
      await store.dispatch("updateName", userJson.name);
      await store.dispatch("updateEmail", userJson.email);
      await store.dispatch("updateAccessToken", userJson.accessToken);
    }
    return response;
  },
  async (error) => {
    if (error.response?.data?.message === "Token has been revoked") {
      await store.dispatch("logout");
    }
    return error;
  }
);

export default axios;
