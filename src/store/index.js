import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";
import axios from "axios";

const updateState = (
  commit,
  status,
  username,
  accessToken,
  refreshToken,
  message
) => {
  commit("setStatus", status);
  commit("setUsername", username);
  commit("accessToken", accessToken);
  commit("refreshToken", refreshToken);
  commit("setMessage", message);
};

export default createStore({
  state: {
    statusLogin: "",
    username: "",
    accessToken: "",
    refreshToken: "",
    message: "",
    rememberAccount: false,
  },
  getters: {
    statusLogin: (state) => state.statusLogin,
    username: (state) => state.username,
    accessToken: (state) => state.accessToken,
    refreshToken: (state) => state.refreshToken,
    message: (state) => state.message,
    rememberAccount: (state) => state.rememberAccount,
  },
  mutations: {
    setStatus(state, statusLogin) {
      state.statusLogin = statusLogin;
    },
    setUsername(state, username) {
      state.username = username;
    },
    accessToken(state, accessToken) {
      state.accessToken = accessToken;
    },
    refreshToken(state, refreshToken) {
      state.refreshToken = refreshToken;
    },
    setMessage(state, message) {
      state.message = message;
    },
    setRememberAccount(state, rememberAccount) {
      state.rememberAccount = rememberAccount;
    },
  },
  actions: {
    configRememberAccount({ commit }, rememberAccount) {
      commit("setRememberAccount", rememberAccount);
    },
    async login({ commit }, payload) {
      const result = {};
      try {
        const response = await axios.post(
          `${process.env.BACKEND_URL}/login`,
          payload
        );
        if (response?.data?.statusCode == 10000) {
          updateState(
            commit,
            "lmao",
            response.data.data.username,
            response.data.data.accessToken,
            response.data.data.refreshToken
          );
          // commit("setStatus", "lmao");
          // commit("setUsername", response.data.data.username);
          // commit("accessToken", response.data.data.accessToken);
          // commit("refreshToken", response.data.data.refreshToken);
          // commit("setMessage", "Success");
          document.cookie = `refreshToken=${response.data.data.refreshToken}; SameSite=Strict; Path=/`;
          result.status = "ok";
          result.data = {
            username: response.data.data.username,
            accessToken: response.data.data.accessToken,
            refreshToken: response.data.data.refreshToken,
          };
        } else {
          throw new Error(response?.response?.data?.message);
        }
      } catch (error) {
        const message = error.response ? error?.response?.data?.message : error;
        updateState(commit, "error", "", "", "", message);
        // commit("setStatus", "error");
        // commit("setUsername", "");
        // commit("accessToken", "");
        // commit("refreshToken", "");
        // commit("setMessage", message);
        result.status = "error";
        result.message = message;
      } finally {
        return result;
      }
    },
    async register({ commit }, payload) {
      const result = {};
      try {
        const response = await axios.post(
          `${process.env.BACKEND_URL}/register`,
          payload
        );
        if (response?.data?.statusCode == 10000) {
          updateState(
            commit,
            "lmao",
            response.data.data.username,
            response.data.data.accessToken,
            response.data.data.refreshToken,
            "Success"
          );
          // commit("setStatus", "lmao");
          // commit("setUsername", response.data.data.username);
          // commit("accessToken", response.data.data.accessToken);
          // commit("refreshToken", response.data.data.refreshToken);
          // commit("setMessage", "Success");
          document.cookie = `refreshToken=${response.data.data.refreshToken}; SameSite=Strict; Path=/`;
          result.status = "ok";
          result.data = {
            username: response.data.data.username,
            accessToken: response.data.data.accessToken,
            refreshToken: response.data.data.refreshToken,
          };
          return result;
        } else {
          throw new Error(response?.response?.data?.message);
        }
      } catch (error) {
        const message = error.response ? error?.response?.data?.message : error;
        updateState(commit, "error", "", "", "", message);
        // commit("setStatus", "error");
        // commit("setUsername", "");
        // commit("accessToken", "");
        // commit("refreshToken", "");
        // commit("setMessage", message);
        result.status = "error";
        result.message = message;
      } finally {
        return result;
      }
    },
    async logout({ commit }) {
      updateState(commit, "", "", "", "", "");
      // commit("setStatus", "");
      // commit("setUsername", "");
      // commit("accessToken", "");
      // commit("refreshToken", "");
      // commit("setMessage", "");
      document.cookie = `refreshToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
      try {
        await axios.post(`${process.env.BACKEND_URL}/logout`, {
          refreshToken: this.state.refreshToken,
        });
      } catch (error) {
        console.log(error);
      }
    },
    async updateAccessToken({ commit }, newAccessToken) {
      commit("accessToken", newAccessToken);
    },
  },
  plugins: [createPersistedState()],
  modules: {},
});
