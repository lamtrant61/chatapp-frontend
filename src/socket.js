import { io } from "socket.io-client";

const socket = io(process.env.SOCKET_URL, {
  withCredentials: true, // Send cookies in the request
  transports: ["websocket"], // Use websocket for communication
});

export default socket;
